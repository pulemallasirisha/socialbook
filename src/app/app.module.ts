import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeftsideComponent } from './leftside/leftside.component';
import { RighsideComponent } from './righside/righside.component';
import { MiddlesideComponent } from './middleside/middleside.component';
import { NavComponent } from './nav/nav.component';
import { SettingsComponent } from './settings/settings.component';

@NgModule({
  declarations: [
    AppComponent,
    LeftsideComponent,
    RighsideComponent,
    MiddlesideComponent,
    NavComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
