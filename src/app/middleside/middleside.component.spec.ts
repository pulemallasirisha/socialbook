import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiddlesideComponent } from './middleside.component';

describe('MiddlesideComponent', () => {
  let component: MiddlesideComponent;
  let fixture: ComponentFixture<MiddlesideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MiddlesideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MiddlesideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
