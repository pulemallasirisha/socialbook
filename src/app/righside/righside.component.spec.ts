import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RighsideComponent } from './righside.component';

describe('RighsideComponent', () => {
  let component: RighsideComponent;
  let fixture: ComponentFixture<RighsideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RighsideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RighsideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
